/*
 * @author S�ren Fischer
 * @version 1.0
 */
public class Person {
	
	private String name;
	private int alter;
	
	public Person() {
		}
	
	public Person(String name, int alter) {
		this.name =name;
		this.alter = alter;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		this.alter = alter;
	}
	
	public String toString() {
		return "Klasse: Person\n" +
				"Name: " + this.name +"\n" +
				"Alter: " + this.alter + "\n";
	}
	
	
}
		

