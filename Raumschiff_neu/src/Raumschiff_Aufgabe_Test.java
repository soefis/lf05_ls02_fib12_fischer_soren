public class Raumschiff_Aufgabe_Test {
	
	public class RaumschiffTest {
	    public static void main(String[] args){

	            Raumschiff klingonen = new Raumschiff(
	                    1,
	                    100,
	                    100,100,
	                    100,
	                    2,
	                    "IKS Hegh'ta",
	                    null,
	                    null
	            );

	            Raumschiff romulaner = new Raumschiff(
	                    2,
	                    100,
	                    100,
	                    100,
	                    100,
	                    2,
	                    "IRW Khazara",
	                    null ,
	                    null
	            );
	            
	            Raumschiff vulkanier = new Raumschiff(
	                    0,
	                    80,
	                    80,
	                    50,
	                    100,
	                    2,
	                    "Ni�Var",
	                    null,
	                    null
	            );

	            Ladung klingonenLadung1 = new Ladung(
	                    "Ferengi Schneckensaft",
	                    200
	            );

	            Ladung klingonenLadung2 = new Ladung(
	                    "Bat'leth Klingonen Schwert",
	                    200
	            );

	            Ladung romulanerLadung1A = new Ladung(
	                    "Borg-Schrott",
	                    5
	            );

	            Ladung romulanerLadung1B = new Ladung(
	                    "Rote Marine",
	                    2
	            );

	            Ladung romulanerLadung2 = new Ladung(
	                    "Plasma-Waffe",
	                    50
	            );

	            Ladung vulkanierLadung1 = new Ladung(
	                    "Forschungssonde",
	                    35
	            );

	            Ladung vulkanierLadung2 = new Ladung(
	                    "Photonentorpedo",
	                    3
	            );
	            System.out.println(vulkanier);
	    }
	}

}
