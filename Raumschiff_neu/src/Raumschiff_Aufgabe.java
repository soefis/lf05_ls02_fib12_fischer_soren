
public class Raumschiff_Aufgabe {

		
		int photonentorpedoAnzahl;
		int energierversorgunginProzent;
		int schildeinProzent;
		int huelleinProzent;
		int lebenserhaltungssystemeinProzent;
		int androidenAnzahl;
		String schiffsname;
		
		 private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
		    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
		    
		public int getPhotonentorpedoAnzahl() {
			return photonentorpedoAnzahl;
		}
		public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		}
		public int getEnergierversorgunginProzent() {
			return energierversorgunginProzent;
		}
		public void setEnergierversorgunginProzent(int energierversorgunginProzent) {
			this.energierversorgunginProzent = energierversorgunginProzent;
		}
		public int getSchildeinProzent() {
			return schildeinProzent;
		}
		public void setSchildeinProzent(int schildeinProzent) {
			this.schildeinProzent = schildeinProzent;
		}
		public int getHuelleinProzent() {
			return huelleinProzent;
		}
		public void setHuelleinProzent(int huelleinProzent) {
			this.huelleinProzent = huelleinProzent;
		}
		public int getLebenserhaltungssystemeinProzent() {
			return lebenserhaltungssystemeinProzent;
		}
		public void setLebenserhaltungssystemeinProzent(int lebenserhaltungssystemeinProzent) {
			this.lebenserhaltungssystemeinProzent = lebenserhaltungssystemeinProzent;
		}
		public int getAndroidenAnzahl() {
			return androidenAnzahl;
		}
		public void setAndroidenAnzahl(int androidenAnzahl) {
			this.androidenAnzahl = androidenAnzahl;
		}
		public String getSchiffsname() {
			return schiffsname;
		}
		public void setSchiffsname(String schiffsname) {
			this.schiffsname = schiffsname;
		}
		
		

}
